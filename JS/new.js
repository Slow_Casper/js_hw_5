
// const newUser = {};

function createNewUser() {
    let firstName = prompt("Введіть ваше ім'я");
    let lastName = prompt("Введіть ваше прізвище");
    const newUser = {
        set firstName(firstName) {
            this._firstName = firstName;
        },
        get firstName() {
            return this._firstName
        },
        set lastName(lastName) {
            this._lastName = lastName;
        },
        get lastName() {
            return this._lastName
        },
        getLogin(){
            return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`
        },
        
    };
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());